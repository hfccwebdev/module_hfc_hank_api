<?php

namespace Drupal\hfc_hank_api;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Template\Attribute;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

/**
 * Defines the HANK API.
 *
 * @ingroup hfcc_modules
 */
class HfcHankApi implements HfcHankApiInterface {

  use LoggerChannelTrait;
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Stores the Configuration Factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  private $config;

  /**
   * Stores the GuzzleHttp Client service.
   *
   * @var \GuzzleHttp\Client
   */
  private $httpClient;

  /**
   * Stores the Cache Backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private $cache;

  /**
   * Creates Constructor for these objects.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The Immutable config.
   * @param \GuzzleHttp\Client $http_client
   *   The Guzzle HTTP Client.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The Cache Backend Interface.
   */
  public function __construct(
    ConfigFactory $config_factory,
    Client $http_client,
    CacheBackendInterface $cache
  ) {
    $this->config = $config_factory->get('hfc_hank_api.settings');
    $this->httpClient = $http_client;
    $this->cache = $cache;
  }

  /**
   * {@inheritdoc}
   */
  public function getData($endpoint, $value = NULL, array $query = [], $cache = TRUE) {
    if ($connection = $this->config->get('connection_path')) {
      if ($secret = $this->config->get('secret')) {
        $query['key'] = $secret;
        return $this->request($connection, $endpoint, $value, $query, $cache);
      }
      else {
        $this->messenger()->addMessage($this->t('HANK API secret not set. Please configure <a href="/admin/config/hfc/hfc-hank-api">API settings</a>.'));
      }
    }
    else {
      $this->messenger()->addMessage($this->t('HANK API connection not set. Please configure <a href="/admin/config/hfc/hfc-hank-api">API settings</a>.'));
    }
    return FALSE;
  }

  /**
   * Query the API and process the results.
   */
  private function request($connection, $endpoint, $value, $query, $cache) {

    $url = $connection . '/' . $endpoint . '/' . rawurlencode($value) . '?' . UrlHelper::buildQuery($query);
    $cid = "hfc_hank_api_$url";
    if ($cache && $result = $this->cache->get($cid)) {
      return $result->data;
    }
    $response = $this->httpClient->request('GET', $url, [RequestOptions::TIMEOUT => 5]);
    if ($response->getStatusCode() == 200) {
      $data = json_decode($response->getBody());
      if (!empty($response->getBody())) {
        if ($cache) {
          $this->cache->set($cid, $data);
        }
        return $data;
      }
    }
    else {
      $this->messenger()->addMessage($this->t('An error occurred retrieving HANK data. Please try again later.'));
      $this->getLogger('hfc_hank_api')->error('Error %code on HANK API %endpoint reading %value', [
        '%code' => $response->getStatusCode(),
        '%endpoint' => $endpoint,
        '%value' => $value,
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getPhoto($value) {
    if ($connection = $this->config->get('connection_path')) {
      if ($secret = $this->config->get('secret')) {
        $query['key'] = $secret;
        $url = $connection . '/pictures/' . $value . '?' . UrlHelper::buildQuery($query);
        $response = $this->httpClient->request('GET', $url, [RequestOptions::TIMEOUT => 5]);
        if ($response->getStatusCode() == 200 && !empty($response->getBody())) {
          $image = [
            'src' => "data:image/jpg;base64," . base64_encode($response->getBody()),
            'alt' => 'id photo',
          ];
          return '<img' . new Attribute($image) . ' />';
        }
      }
      else {
        $this->messenger()->addMessage($this->t('HANK API secret not set. Please configure <a href="/admin/config/hfc/hfc-hank-api">API settings</a>.'));
      }
    }
    else {
      $this->messenger()->addMessage($this->t('HANK API connection not set. Please configure <a href="/admin/config/hfc/hfc-hank-api">API settings</a>.'));
    }
    return FALSE;
  }

}
