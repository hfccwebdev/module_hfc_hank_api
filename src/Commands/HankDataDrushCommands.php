<?php

namespace Drupal\hfc_hank_api\Commands;

use Drupal\hfc_hank_api\HfcHankApi;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class HankDataDrushCommands extends DrushCommands {

  /**
   * The account switcher service.
   *
   * @var \Drupal\hfc_hank_api\HfcHankApi
   */
  protected $hfcHankApi;

  /**
   * Commands constructor.
   *
   * @param \Drupal\hfc_hank_api\HfcHankApi $hfcHankApi
   *   Catalog Archive Builder Service.
   */
  public function __construct(
    HfcHankApi $hfcHankApi
  ) {
    $this->hfcHankApi = $hfcHankApi;
  }

  /**
   * Gets HANK API data.
   *
   * @param string $endpoint
   *   The HANK API endpoint to load.
   * @param mixed $id
   *   Optional id.
   *
   * @command hankapi:get
   * @aliases hankapi-get
   */
  public function getData(string $endpoint, $id = NULL) {
    print_r($this->hfcHankApi->getData($endpoint, $id));
  }

}
