<?php

namespace Drupal\hfc_hank_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the HFC HANK API settings form.
 *
 * @package Drupal\hfc_hank_api\Form
 */
class AdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'hfc_hank_api.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hfc_hank_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('hfc_hank_api.settings');

    $form['connection_path'] = [
      '#title' => 'HANK API Connection',
      '#type' => 'textfield',
      '#default_value' => $config->get('connection_path'),
      '#description' => $this->t('The URL of the connection. Do not include a trailing slash.'),
      '#required' => TRUE,
    ];

    $form['secret'] = [
      '#title' => 'HANK API Secret',
      '#type' => 'textfield',
      '#default_value' => $config->get('secret'),
      '#description' => $this->t('The secret value to pass with the request.'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('hfc_hank_api.settings')
      ->set('connection_path', $form_state->getValue('connection_path'))
      ->set('secret', $form_state->getValue('secret'))
      ->save();
  }

}
