<?php

namespace Drupal\hfc_hank_api;

/**
 * Defines the HANK API Interface.
 *
 * @ingroup hfcc_modules
 */
interface HfcHankApiInterface {

  /**
   * Query the API.
   *
   * @param string $endpoint
   *   The API endpoint name.
   * @param string $value
   *   The API key value.
   * @param array $query
   *   An array of additional query parameters. (optional)
   * @param bool $cache
   *   Allow system to cache results.
   *
   * @return \stdClass[]
   *   An array of objects.
   */
  public function getData($endpoint, $value = NULL, array $query = [], $cache = TRUE);

  /**
   * Fetch a photo from the API.
   *
   * @param string $hankid
   *   The person's HANK ID.
   *
   * @return string
   *   An embedded HTML image.
   */
  public function getPhoto($hankid);

}
